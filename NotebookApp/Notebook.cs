﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NotebookApp
{
    class Notebook
    {
        static Dictionary<int,Note> listNote = new Dictionary<int, Note>();
        static bool on = true;
        static string pattern = @"^\s*\+?\s*([0-9][\s-]*){9,}$";
        static void Main(string[] args)
        {
            while (on)
            {
                Console.Clear();
                Console.WriteLine("Выберете пункт меню:");
                Console.WriteLine("1. Создать новую запись");
                Console.WriteLine("2. Редактировать запись");
                Console.WriteLine("3. Удалить запись");
                Console.WriteLine("4. Просмотр всех записей");
                Console.WriteLine("5. Просмотр конкретной записи");
                Console.WriteLine("6. Выход");
                string a = Console.ReadLine();
                switch (a)
                {
                    case "1":
                        AddNote();
                        break;
                    case "2":
                        EditNote();
                        break;
                    case "3":
                        DeleteNote();
                        break;
                    case "4":
                        ShowNotes();
                        break;
                    case "5":
                        ShowNote();
                        break;
                    case "6":
                        on = false;
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Вы ввели что-то не то... Попробуйте еще раз");
                        break;

                }
            }
        }

        static void AddNote()
        {
            string s;
            Console.Clear();
            Note note = new Note();
            do
            {
                Console.Write("Введите фамилию: ");
                s = Console.ReadLine();
            }
            while (s == "");
            note.Surname = s;
            do
            {
                Console.Write("Введите имя: ");
                s = Console.ReadLine();
            }
            while (s == "");
            note.Name = s;
            Console.Write("Введите отчество (не обязательно): ");
            s = Console.ReadLine();
            if (s != "")
            {
                note.SecondName = s;
            }
            while(true)
            {
                Console.Write("Введите номер телефона (не менее 9 цифр): ");
                s = Console.ReadLine();
                if (Regex.IsMatch(s,pattern,RegexOptions.IgnoreCase))
                {
                    note.PhoneNumber = s;
                    break;
                }
            }
            
            Console.Write("Введите страну (не обязательно): ");
            s = Console.ReadLine();
            if (s != "")
            {
                note.Country = s;
            }
            while (true)
            {
                Console.Write("Введите дату рождения (не обязательно): ");
                s = Console.ReadLine();
                if(s=="")
                {
                    break;
                }
                if(DateTime.TryParse(s, out DateTime result))
                {
                    note.DateOfBirth = result;
                    break;
                }
            }
            
           
            Console.Write("Введите организацию (не обязательно): ");
            s = Console.ReadLine();
            if (s != "")
            {
                note.Organization = s;
            }
            Console.Write("Введите должность (не обязательно): ");
            s = Console.ReadLine();
            if (s != "")
            {
                note.Position = s;
            }
            Console.Write("Прочие заметки (не обязательно): ");
            s = Console.ReadLine();
            if (s != "")
            {
                note.OtherNote = s;
            }
            //note.Id = Note.CountNote;
            listNote.Add(Note.CountNote,note);
            Console.WriteLine("Нажмите любую клавишу, чтобы вернуться в меню");
            Console.ReadKey();
        }
        static void EditNote()
        {
            bool i;
            Console.Clear();
            Console.WriteLine("Укажите номер записи, которую хотите редактировать");
            int id = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Что вы хотите редактировать:");
            Console.WriteLine("1. Фамилия");
            Console.WriteLine("2. Имя");
            Console.WriteLine("3. Отчество");
            Console.WriteLine("4. Номер телефона");
            Console.WriteLine("5. Страна");
            Console.WriteLine("6. Дата рождения");
            Console.WriteLine("7. Организация");
            Console.WriteLine("8. Должность");
            Console.WriteLine("9. Прочие заметки");
            Console.Write("ВВЕДИТЕ НОМЕР:");
            int n = Convert.ToInt32(Console.ReadLine());
            do
            {
                i = false;
                string newval;
                switch (n)
                {
                    case 1:
                        do
                        {
                            Console.Write("Введите новое значение Фамилии(обязательное поле):");
                            newval = Console.ReadLine();
                            listNote[id].Surname = newval;
                        }
                        while (newval == "");
                        break;
                    case 2:
                        do
                        {
                            Console.Write("Введите новое значение Имени(обязательное поле):");
                            newval = Console.ReadLine();
                            listNote[id].Name = newval;
                        }
                        while (newval == "");
                        break;
                    case 3:
                        Console.Write("Введите новое значение Отчества:");
                        listNote[id].SecondName = Console.ReadLine(); 
                        break;
                    case 4:
                        while (true)
                        {
                            Console.Write("Введите новое значение Номера телефона (не менее 9 цифр): ");
                            newval= Console.ReadLine();
                            if (Regex.IsMatch(newval, pattern, RegexOptions.IgnoreCase))
                            {
                                listNote[id].PhoneNumber = newval;
                                break;
                            }
                        }                        
                        break;
                    case 5:
                        Console.Write("Введите новое значение Страны:");
                        listNote[id].Country = Console.ReadLine();
                        break;
                    case 6:
                        while(true)
                        { 
                            Console.WriteLine("Введите новое значение Даты рождения");
                            newval = Console.ReadLine();
                            if (DateTime.TryParse(newval, out DateTime result))
                            {
                                listNote[id].DateOfBirth = result;
                                break;
                            }
                            if(newval=="")
                            {
                                listNote[id].DateOfBirth = DateTime.MinValue;
                                break;
                            }
                        }
                        break;
                    case 7:
                        Console.Write("Введите новое значение Организации:");
                        listNote[id].Organization = Console.ReadLine();
                        break;
                    case 8:
                        Console.Write("Введите новое значение Должности:");
                        listNote[id].Position = Console.ReadLine();
                        break;
                    case 9:
                        Console.Write("Введите прочие заметки:");
                        listNote[id].OtherNote = Console.ReadLine();
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Вы ввели что-то не то... Попробуйте еще раз");
                        i = true;
                        break;
                }
            }
            while (i);

        }
        static void DeleteNote()
        {
            Console.Clear();
            while (true)
            {
                Console.WriteLine("Укажите номер записи, которую хотите удалить");
                int n = Convert.ToInt32(Console.ReadLine());
                if (listNote.ContainsKey(n) == true)
                {
                    listNote.Remove(n);
                    Console.WriteLine("Запись удалена!");
                    break;
                }
                else
                {
                    Console.WriteLine( "Записи с таким номером не сущетсвует!");
                }                
            }
            Console.WriteLine("Нажмите любую клавишу, чтобы вернуться в меню");
            Console.ReadKey();
        }
        static void ShowNotes()
        {
            Console.Clear();
            foreach (var item in listNote)
            {
                Console.WriteLine("Запись №"+item.Key);
                Console.WriteLine(item.Value);
            }
            Console.WriteLine("Нажмите любую клавишу, чтобы вернуться в меню");
            Console.ReadKey();
        }
        static void ShowNote()
        {
            Console.Clear();
            Console.WriteLine("Укажите номер записи, которую хотите посмотреть");
            int n = Convert.ToInt32(Console.ReadLine());
            if (listNote.ContainsKey(n) == true)
            {
                Console.WriteLine("Запись №" + n);
                Console.WriteLine("Фамилия: " + listNote[n].Surname);
                Console.WriteLine("Имя: " + listNote[n].Name);
                if (listNote[n].SecondName != null)
                {
                    Console.WriteLine("Отчество:" + listNote[n].SecondName);
                }
                Console.WriteLine("Номер телефона: " + listNote[n].PhoneNumber);
                if (listNote[n].Country != null)
                {
                    Console.WriteLine("Страна: " + listNote[n].Country);
                }
                if (listNote[n].DateOfBirth != DateTime.MinValue)
                {
                    Console.WriteLine("Дата рождения: " +listNote[n].DateOfBirth.ToShortDateString());
                }
                if (listNote[n].Organization != null)
                {
                    Console.WriteLine("Организация: " + listNote[n].Organization);
                }
                if (listNote[n].Position != null)
                {
                    Console.WriteLine("Должность: " + listNote[n].Position);
                }
                if (listNote[n].OtherNote != null)
                {
                    Console.WriteLine("Прочие заметки: " + listNote[n].OtherNote);
                }
                Console.WriteLine("Нажмите любую клавишу, чтобы вернуться в меню");
                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Записи с таким номером не существует!");
                 Console.WriteLine("Нажмите любую клавишу, чтобы вернуться в меню");
                Console.ReadKey();
            }
        }
    }
}
