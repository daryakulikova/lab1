﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotebookApp
{
    class Note
    {
        public static int CountNote;
        //public int Id { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public string SecondName { get; set; }
        public string PhoneNumber { get; set; }
        public string Country { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Organization { get; set; }
        public string Position { get; set; }
        public string OtherNote { get; set; }

        public Note()
        {
            CountNote++;
        }

        public override string ToString()
        {
            return "Фамилия - " + this.Surname + ", Имя - " + this.Name + ", Номер телефона: " + this.PhoneNumber;
        }
    }
}
